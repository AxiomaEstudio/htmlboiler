@extends('layouts.default')

@section('content')
    <div id="thanks">
        <section class="row">
            <div class="col " align="center">
                <h1 class="txt main-title white">{!! lang('thanks-copy', $page)->title !!}</h1>
                <div class="txt text-big">
                    {!! lang('thanks-copy', $page)->value !!}
                </div>
                <div class="more-wrapper">
                    <a href="{{ route('home') }}" class="cta">
                        {{ lang('go-back')->title }}
                    </a>
                </div>
            </div>
        </section>
    </div>
@stop
