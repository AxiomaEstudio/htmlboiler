<div class="ax-forms newsletter-form">
    <h4>{{ lang('sign-in-newsletter')->title }}</h4>

    <form action="{{ url('newsletter') }}" method="POST">
        {!! csrf_field() !!}
        <input type="text" name="prefered-pet" class="first-name">

        {!! render_input('email', 'email', 'required', 'newsletter-email') !!}

        <div class="check-wrapper">
            <input type="checkbox" name="terms" class="check" required>
            <label for="terms">{{ lang('habes-disclaimer')->title }} <a href="">{{ lang('habes-disclaimer')->value }}</a></label>
        </div>

        <div class="submit-btn">
            <input type="submit" value="{{ lang('form-send')->title }}">
        </div>
    </form>
</div>
