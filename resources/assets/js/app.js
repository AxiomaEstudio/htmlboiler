
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.Cookies = require('cookies-js');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


/* Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.directive('selectric', {
  inserted: function (el, bindings, vnode) {
    $(el).selectric({
      arrowButtonMarkup: '<b class="button-drop"><i class="glyphicon glyphicon-chevron-down"></i></b>',
      disableOnMobile: false,
    })
    .on('selectric-change', function(event, element, selectric) {
    });
  }
})

Vue.directive('slick', {
  // When the bound element is inserted into the DOM...
  bind: function (el) {
    // Focus the element
    $(el).slick();
  }
})
*/

const app = new Vue({
  el: '#search',
  data: {
    results: [],
    string: "",
    loading: false,
  },
  methods: {
    filter: function(number){
      this.loading = true;
      this.products = [];

        axios.post('/search-contents', {
          number: number,
        })
        .then(
            response => {
                this.results = response.data;
                this.loading = false;
            }
        )
        .catch(error => console.log(error));
    },
  },
  created: function(){
    this.loading = true;

    axios.post('/search-contents', {})
    .then(
      response => {
        this.results = response.data;
        this.loading = false;
      }
    )
    .catch(error => console.log(error));
  },
  computed: {
      // A computed property that holds only those results that match the searchString.
      filteredResults: function () {
          var results_array = this.results,
              searchString = this.string;

          if(!searchString){
            results_array = [];
            return results_array;
          }

          searchString = searchString.trim().toLowerCase();

          results_array = results_array.filter(function(item){
              if(item.title.toLowerCase().indexOf(searchString) !== -1) {
                  return item;
              }
          })

          // Return an array with the filtered data.
          return results_array;
      }
  }
});

 // Initiate
$(document).ready(function() {
  sticky('#header');

  $('#go-to-team').click(function(event) {
    animateTo('.team-wrapper', 1000);
    $('.sub-menu li').removeClass('active');
    $(this).addClass('active');
  });

  // Toggle
  $('.toggle-accordion').click(function(event) {
    var id = $(this).attr('id');
    var el = '.' + id;
    var icon = $(this).find('i');

    if (icon.hasClass('fa-caret-right')) {
      icon.removeClass('fa-caret-right');
      icon.addClass('fa-caret-down');
    }else{
      icon.removeClass('fa-caret-down');
      icon.addClass('fa-caret-right');
    }

    openItem(el, '.accordion-item');
  });

  //CKEditor additional CSS
  $('.videodetector').addClass('embed-responsive embed-responsive-16by9');
  $('.videodetector iframe').addClass('embed-responsive-item');

});

// Plugins
$(function() {
  $('select.selectric').selectric({
  arrowButtonMarkup: '<b class="button-drop"></b>',
  disableOnMobile: false,
  });
});

// Resize execution
$(window).resize(function() {
});

// Functions
function sticky(elem) {
  var menu = $(elem);
  var breakpoint = 250;

  if (menu.length) {
    //attaching a scroll event
    $(window).scroll(function () {
      if ($(window).scrollTop() > breakpoint) {
        menu.addClass('sticky fadeInDown');
        $('body').css('padding-top',breakpoint);
      } else {
        menu.removeClass('sticky fadeInDown');
        $('body').css('padding','0 0 0 0');
      }
    });
  };
}

function stickyFixed(elem, scroll) {
  var menu = $(elem);
  var breakpoint = scroll;

  if (menu.length) {
    //attaching a scroll event
    $(window).scroll(function () {
      if ($(window).scrollTop() > breakpoint) {
        menu.addClass('sticky fadeInDown');
      } else {
        menu.removeClass('sticky fadeInDown');
      }
    });
  };
}

function disableSubmit(elem, msg) {
    document.getElementById(elem).value = msg;
    document.getElementById(elem).disabled = true;
    return true;
}

function animateTo(elem, speed = 400) {
  $('html, body').stop().animate({
    'scrollTop': $(elem).offset().top
  }, speed, 'swing');
}

function openItem(el, elems) {
  var elements = $(elems);
  var element  = $(el);

  if (element.hasClass('open')) {
    element.removeClass('open');
    elements.slideUp();
  }else{
    elements.removeClass('open');
    element.addClass('open');
    elements.slideUp();
    element.slideDown();
  }
}
