<?php

Route::get('/', function () {
    return view('frontpages.home');
})->name('home');

Route::get('projects', function () {
    return view('frontpages.projects');
})->name('projects');

Route::get('project', function () {
    return view('contentpages.project');
})->name('project');

Route::get('contact', function () {
    return view('frontpages.contact');
})->name('contact');

Route::get('terms', function () {
    return view('frontpages.terms', ["active" => 'terms']);
})->name('terms');

Route::get('habeas', function () {
    return view('frontpages.terms', ["active" => 'habeas']);
})->name('habeas');
