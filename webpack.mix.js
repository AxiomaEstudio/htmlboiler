let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//Front pages Assets managment
mix.js(
    [
      'resources/assets/js/app.js',
    ],
    'public/js'
  )
  .extract([
    'axios',
    'bootstrap',
    'jquery',
    'chart.js',
    'popper.js',
    'lodash',
    'lightbox2',
    'selectric',
    'slick-carousel',
    'vue',
    'cookies-js',
    './bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js'
  ])
  .autoload({
    jquery: ['$', 'window.jQuery', 'jQuery'],
  })
  .sass('resources/assets/sass/app.scss', 'public/css')
  .styles(
    [
      './node_modules/bootstrap/dist/css/bootstrap.min.css',
      './bower_components/animate.css/animate.min.css',
      './node_modules/slick-carousel/slick/slick.css',
      './node_modules/@fortawesome/fontawesome-free-webfonts/css/fa-brands.css',
      './node_modules/@fortawesome/fontawesome-free-webfonts/css/fa-regular.css',
      './node_modules/@fortawesome/fontawesome-free-webfonts/css/fa-solid.css',
      './node_modules/@fortawesome/fontawesome-free-webfonts/css/fontawesome.css',
      './node_modules/selectric/public/selectric.css',
      './node_modules/lightbox2/dist/css/lightbox.min.css',
    ],
    'public/css/plugins.css'
  )
  .options(
    {
      processCssUrls: false
    }
  );

// Conditionals
if (mix.inProduction()) {
    mix.version();
}
