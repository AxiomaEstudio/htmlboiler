var app = new Vue({
  el: '#search',
  data: {
    results: [],
    string: "",
  },
  methods: {
  },
  created: function(){
    this.loading = true;

    axios.post('/vue-products', {
      page: 'products'
    })
    .then(
      response => {
        this.results = response.data;
        this.loading = false;
      }
    )
    .catch(error => console.log(error));
  },
  computed: {
      // A computed property that holds only those results that match the searchString.
      filteredResults: function () {
          var results_array = this.results,
              searchString = this.string;

          if(!searchString){
            results_array = [];
            return results_array;
          }

          searchString = searchString.trim().toLowerCase();

          results_array = results_array.filter(function(item){
              if(item.title.toLowerCase().indexOf(searchString) !== -1
                ||
                item.description.toLowerCase().indexOf(searchString) !== -1
                ||
                item.category.toLowerCase().indexOf(searchString) !== -1) {
                  return item;
              }
          })

          // Return an array with the filtered data.
          return results_array;
      }
  }
});

Vue.directive('selectric', {
  inserted: function (el, bindings, vnode) {
    $(el).selectric({
      arrowButtonMarkup: '<b class="button-drop"><i class="glyphicon glyphicon-chevron-down"></i></b>',
      disableOnMobile: false,
    })
    .on('selectric-change', function(event, element, selectric) {
    });
  }
})

Vue.directive('slick', {
  // When the bound element is inserted into the DOM...
  bind: function (el) {
    // Focus the element
    $(el).slick();
  }
})

var app = new Vue({
  el: '#products',
  data: {
    results: [],
    string: "",
    page: activePage,
    activeSlug: slug,
    loading: false,
    products: [],
    promoProducts: [],
    error: [],
  },
  methods: {
    filter: function(id, type, number, filter){
      this.loading = true;
      this.products = [];

        axios.post('/vue-products', {
          queryType: type,
          id: id,
          number: number,
          filter: filter
        })
        .then(
            response => {
                this.products = response.data;
                this.loading = false;
            }
        )
        .catch(error => console.log(error));
    },
    categorize: function(id){
      var results_array = this.results;

      results_array = results_array.filter(function(item){
        if(item.categoryId == id) {
            return item;
        }
      })

      this.products = results_array;
    },
    clearFilter: function(){
      this.loading = true;
      this.products = [];

      axios.post('/vue-products', {
            page: this.page
      })
      .then(
          response => {
              this.products = response.data;
              this.loading = false;
          }
      )
      .catch(error => console.log(error));
    }
  },
  created: function(){
    this.products = initialProducts;
    this.promoProducts = initialPromoProducts;
  }
});
