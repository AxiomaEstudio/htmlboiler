@extends('layouts.default')

@section('content')
    <div id="projects">
        <section class="row projects-wrapper">
            @for($i = 0; $i < 4; $i++)
                <div class="col-6 col-sm-4 al-center">
                    <img src="http://via.placeholder.com/250x250">
                    <h2 class="txt header-5 bold">
                        Lorrem ipsum stratos
                    </h2>
                    <div class="txt text grey">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea distinctio quidem cupiditate nisi illum voluptatem alia.
                    </div>
                    <a href="{{ route('project')}}">
                        Leer más
                    </a>
                </div>
            @endfor
        </section>

        <!--footer-->
        @include('sections.footer')
        <!-- /footer-->
    </div>
@stop
