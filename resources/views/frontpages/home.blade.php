@extends('layouts.default')

@section('content')
    <div id="home">
        <section class="row">
            <div class="slider">
                @for($i = 0; $i < 4; $i++)
                    <div class="slide">
                      <img src="http://via.placeholder.com/1920x600">
                        <div class="text">
                            Lorrem
                        </div>
                    </div>
                @endfor
            </div>
        </section>

        <section class="row">
            <div class="col-auto">
                <div class="txt header-1">Text styles ready for testing</div>
                <div class="txt header-2">Text styles ready for testing</div>
                <div class="txt header-3">Text styles ready for testing</div>
                <div class="txt header-4">Text styles ready for testing</div>
                <div class="txt header-5">Text styles ready for testing</div>
                <div class="txt header-6">Text styles ready for testing</div>
                <div class="txt text-big">Text styles ready for testing</div>
                <div class="txt text">Text styles ready for testing</div>
            </div>
        </section>

        <section class="row">
            <div class="col">
                <div id="search">
                    <h2>Lookin for: @{{string}}</h2>
                    <input v-model="string" placeholder="¿?">
                    <ul v-cloak v-if="filteredResults.length != 0">
                        <li v-for="result in filteredResults">
                            <p>@{{result.title}}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <!--footer-->
        @include('sections.footer')
        <!-- /footer-->
	</div>
@stop

@section('scripts')
    <script type="text/javascript">
        $('.slider').slick({
            infinite:true,
            speed:500,
            autoplay: true,
            autoplaySpeed:15000,
            prevArrow:"<div class='slick-prev slick-controls'></div>",
            nextArrow:"<div class='slick-next slick-controls'></div>",
            cssEase: 'linear',
            adaptiveHeight: true,
            fade: false,
            dots:true,
            arrows: true,
            slidesToShow:1,
            slidesToScroll: 1,
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    arrows: false
                  }
                }
            ],
        });
    </script>
@stop
