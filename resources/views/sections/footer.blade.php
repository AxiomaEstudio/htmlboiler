<section class="row justify-content-center">
    <div class="col-md-auto">
        @include('includes.newsletter')
    </div>
</section>

<section class="row justify-content-center">
    <div class="col-auto">
        <p>{{ lang('devel-and-design')->title }} <i class="fas fa-heart"></i></p>
    </div>
</section>
