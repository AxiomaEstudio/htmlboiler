@extends('layouts.default')

@section('content')
    <div id="contact">
        <section class="row justify-content-center">
            <div class="col-auto">
                <div class="ax-forms contact-form">
                    <div class="main-title">
                        <h2>{!! nl2br(lang('formulario de contacto')->title) !!}</h2>
                    </div>

                    <form action="{{ url('contact') }}" method="POST" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <input type="text" name="prefered-pet" class="first-name">

                        {!! render_input('text', 'firstname', 'required', 'form-firstname') !!}
                        {!! render_input('text', 'lastname', 'required', 'form-lasttname') !!}
                        {!! render_input('email', 'email', 'required', 'form-email') !!}
                        {!! render_input('number', 'phone', 'required', 'form-phone') !!}
                        {!! render_input('textarea', 'message', 'required', 'form-message') !!}

                        <select name="" id="" class="selectric">
                            <option value="" checked>Asunto</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>

                        <div class="check-wrapper">
                            <input type="checkbox" name="terms" class="check" required>
                            <label for="terms">{{ lang('habes-disclaimer')->title }} <a href="{{ route('home') }}">{{ lang('habes-disclaimer')->value }}</a></label>
                        </div>

                        <div class="submit-btn">
                            <input type="submit" value="{{ lang('form-send')->title }}">
                        </div>
                    </form>
                </div>
            </div>
        </section>

        <!--footer-->
        @include('sections.footer')
        <!-- /footer-->
    </div>
@stop
