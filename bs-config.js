
/*
 |--------------------------------------------------------------------------
 | Browser-sync config file
 |--------------------------------------------------------------------------
 |
 | For up-to-date information about the options:
 |   http://www.browsersync.io/docs/options/
 |
 | There are more options than you see here, these are just the ones that are
 | set internally. See the website for more info.
 |
 |
 */
module.exports = {
    "ui": {
        "port": 3001
    },
   "files": `resources/views,
            resources/assets/js,
            public/css/app.css,
            public/cms/css/cms.css,
            app,
            routes`,
    "server": false,
    "proxy": 'http://cms.localhost',
    "port": 3000,
    "open": "local",
    "notify": true,
    "online": false,
    "injectChanges": true,
};
