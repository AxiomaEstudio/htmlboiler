<?php
function lang($key, $page = null) {
    $lang = ['title' => $key, 'value' => $key];
    return (object) $lang;
}

/**
*
* return INPUT, TEXTAREA tags
*/
function render_input($type, $name, $required, $lang = null, $description = null) {
    if($lang == null) {
        $lang = $name;
    }

    if($type == "textarea") {
        $input = '<textarea name="'. $name . '" placeholder="'. lang($lang)->title . '" ' . $required . '>' . old($name) . '</textarea>';
    }
    elseif ($type == "file") {
        $input = '<div class="file-input">
                  <input type="'. $type . '" name="'. $name . '" placeholder="'. lang($lang)->title . '" value="'. old($name) . '" ' . $required . '>
                  <p>' .  lang($description)->title . '</p>
                  </div>';
    }
    else {
        $input = '<input type="'. $type . '" name="'. $name . '" placeholder="'. lang($lang)->title . '" value="'. old($name) . '" ' . $required . '>';
    }

    return $input;
}
