webpackJsonp([1],{

/***/ 152:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(153);
module.exports = __webpack_require__(175);


/***/ }),

/***/ 153:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

__webpack_require__(154);

window.Vue = __webpack_require__(23);
window.Cookies = __webpack_require__(24);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/* Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.directive('selectric', {
  inserted: function (el, bindings, vnode) {
    $(el).selectric({
      arrowButtonMarkup: '<b class="button-drop"><i class="glyphicon glyphicon-chevron-down"></i></b>',
      disableOnMobile: false,
    })
    .on('selectric-change', function(event, element, selectric) {
    });
  }
})

Vue.directive('slick', {
  // When the bound element is inserted into the DOM...
  bind: function (el) {
    // Focus the element
    $(el).slick();
  }
})
*/

var app = new Vue({
  el: '#search',
  data: {
    results: [],
    string: "",
    loading: false
  },
  methods: {
    filter: function filter(number) {
      var _this = this;

      this.loading = true;
      this.products = [];

      axios.post('/search-contents', {
        number: number
      }).then(function (response) {
        _this.results = response.data;
        _this.loading = false;
      }).catch(function (error) {
        return console.log(error);
      });
    }
  },
  created: function created() {
    var _this2 = this;

    this.loading = true;

    axios.post('/search-contents', {}).then(function (response) {
      _this2.results = response.data;
      _this2.loading = false;
    }).catch(function (error) {
      return console.log(error);
    });
  },
  computed: {
    // A computed property that holds only those results that match the searchString.
    filteredResults: function filteredResults() {
      var results_array = this.results,
          searchString = this.string;

      if (!searchString) {
        results_array = [];
        return results_array;
      }

      searchString = searchString.trim().toLowerCase();

      results_array = results_array.filter(function (item) {
        if (item.title.toLowerCase().indexOf(searchString) !== -1) {
          return item;
        }
      });

      // Return an array with the filtered data.
      return results_array;
    }
  }
});

// Initiate
$(document).ready(function () {
  sticky('#header');

  $('#go-to-team').click(function (event) {
    animateTo('.team-wrapper', 1000);
    $('.sub-menu li').removeClass('active');
    $(this).addClass('active');
  });

  // Toggle
  $('.toggle-accordion').click(function (event) {
    var id = $(this).attr('id');
    var el = '.' + id;
    var icon = $(this).find('i');

    if (icon.hasClass('fa-caret-right')) {
      icon.removeClass('fa-caret-right');
      icon.addClass('fa-caret-down');
    } else {
      icon.removeClass('fa-caret-down');
      icon.addClass('fa-caret-right');
    }

    openItem(el, '.accordion-item');
  });

  //CKEditor additional CSS
  $('.videodetector').addClass('embed-responsive embed-responsive-16by9');
  $('.videodetector iframe').addClass('embed-responsive-item');
});

// Plugins
$(function () {
  $('select.selectric').selectric({
    arrowButtonMarkup: '<b class="button-drop"></b>',
    disableOnMobile: false
  });
});

// Resize execution
$(window).resize(function () {});

// Functions
function sticky(elem) {
  var menu = $(elem);
  var breakpoint = 250;

  if (menu.length) {
    //attaching a scroll event
    $(window).scroll(function () {
      if ($(window).scrollTop() > breakpoint) {
        menu.addClass('sticky fadeInDown');
        $('body').css('padding-top', breakpoint);
      } else {
        menu.removeClass('sticky fadeInDown');
        $('body').css('padding', '0 0 0 0');
      }
    });
  };
}

function stickyFixed(elem, scroll) {
  var menu = $(elem);
  var breakpoint = scroll;

  if (menu.length) {
    //attaching a scroll event
    $(window).scroll(function () {
      if ($(window).scrollTop() > breakpoint) {
        menu.addClass('sticky fadeInDown');
      } else {
        menu.removeClass('sticky fadeInDown');
      }
    });
  };
}

function disableSubmit(elem, msg) {
  document.getElementById(elem).value = msg;
  document.getElementById(elem).disabled = true;
  return true;
}

function animateTo(elem) {
  var speed = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 400;

  $('html, body').stop().animate({
    'scrollTop': $(elem).offset().top
  }, speed, 'swing');
}

function openItem(el, elems) {
  var elements = $(elems);
  var element = $(el);

  if (element.hasClass('open')) {
    element.removeClass('open');
    elements.slideUp();
  } else {
    elements.removeClass('open');
    element.addClass('open');
    elements.slideUp();
    element.slideDown();
  }
}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(5)))

/***/ }),

/***/ 154:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__webpack_provided_window_dot_jQuery) {
window._ = __webpack_require__(13);
window.Popper = __webpack_require__(11).default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
  window.$ = __webpack_provided_window_dot_jQuery = __webpack_require__(5);

  __webpack_require__(15);
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = __webpack_require__(16);

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

var token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
  window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
  console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(5)))

/***/ }),

/***/ 175:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })

},[152]);