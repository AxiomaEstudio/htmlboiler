@extends('layouts.default')

@section('content')
    <div id="terms">
        <section class="row justify-content-center">
            <div class="content col-10 col-sm-9 col-md-8 col-lg-7 col-xl-6">
                @if($active == 'terms')
                    <h1 class="main-title bold">{!! lang('terms')->title !!}</h1>

                    <div class="txt text">
                        {!! lang('terms')->value !!}
                    </div>
                @elseif($active == 'habeas')
                    <h1 class="main-title bold">{!! lang('habeas')->title !!}</h1>

                    <div class="txt text">
                        {!! lang('habeas')->value !!}
                    </div>
                @endif
            </div>
        </section>
    </div>
    <!--footer-->
    @include('sections.footer')
    <!-- /footer-->
@stop
