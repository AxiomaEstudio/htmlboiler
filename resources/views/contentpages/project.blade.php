@extends('layouts.default')

@section('content')
    <div id="project">
        <section class="row justify-content-center project-wrapper">
            <div class="col-10">
                <img src="http://via.placeholder.com/250x250">
                <h2 class="txt header-5 bold">
                    Lorrem ipsum stratos
                </h2>
                <div class="txt text grey-dark">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea distinctio quidem cupiditate nisi illum voluptatem alia.
                </div>
            </div>
        </section>

        <!--footer-->
        @include('sections.footer')
        <!-- /footer-->
    </div>
@stop
