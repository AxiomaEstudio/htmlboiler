<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
    <head>
        <meta charset="utf-8">
        <title>Title</title>
        <meta name="description" content="Lorem">
        <meta name="author" content="Axioma Estudio SAS">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width,minimum-scale=1, initial-scale=1">
        <link rel="apple-touch-icon" sizes="180x180" href="/fav-180x180.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/fav-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/fav-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/fav.svg" color="#000000">
        <meta name="theme-color" content="#ffffff">
        <!-- Style loading -->
        <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

        @include('scripts.headscripts')
    </head>
    <body class="">
        @include('scripts.analytics')

        <div id="axio-page" class="container-fluid">
            @include('sections.header')
            @yield('content')
        </div>

        <!-- Bundled scripts -->
        <script type="text/javascript" src="{{  mix('/js/manifest.js') }}"></script>
        <script type="text/javascript" src="{{  mix('/js/vendor.js') }}"></script>
        <script type="text/javascript" src="{{  mix('/js/app.js') }}"></script>

        <!-- Views defined scripts -->
        @yield('scripts')

    </body>
</html>
