<?php
    $menuItems = [
        ['home', 'home', lang('menu-home')->title,'no'],
        ['projects', 'projects', lang('menu-projects')->title,'no'],
        ['contact', 'contact', lang('menu-contact')->title,'no'],
        ['terms', 'terms', lang('menu-terms')->title,'no'],
        ['habeas', 'habeas', lang('menu-habeas')->title,'no'],
    ];
?>

<nav class="navbar navbar-expand-lg">
    <div class="menu-toggle-wrapper">
        <button class="menu-toggle" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="line"></span>
            <span class="line"></span>
            <span class="line"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="mainNav">
        <div class="main">
            <div class="col col-12 col-md-4" align="center">
                <a href="{{ url('/') }}">
                    <img src="http://via.placeholder.com/250x100" title="{{ config('app.name')}}" alt="{{ config('app.name')}}" class="logotype">
                </a>
            </div>
            <div class="col col-12 col-md-8">
                <ul class="main-menu">
                    @foreach ($menuItems as $item)
                        <li class="menu-item {{ (isset($active) && $active == $item[0])? 'active' : $item[0] }}">
                            <a href="{{ route($item[1]) }}">{!! $item[2] !!}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</nav>
